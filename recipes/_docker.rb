#
# Cookbook:: bauherr
# Recipe:: _docker

# Install Docker
if node['bauherr']['docker']['install'] && node['kernel']['name'] == 'Linux' && node['kernel']['machine'] == 'x86_64'
  docker_installation_package 'default' do
    version node['bauherr']['docker']['version']
    setup_docker_repo true
    action :create
    package_options %q|--force-yes -o Dpkg::Options::='--force-confold' -o Dpkg::Options::='--force-all'| # if Ubuntu for example
  end
  docker_service 'default' do
    action [:create, :start]
  end
else # check that Docker is required version
  # TODO
end
