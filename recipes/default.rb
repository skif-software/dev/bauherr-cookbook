#
# Cookbook:: bauherr
# Recipe:: default

# Bauherr can be installed either on a single machine or in the Docker Swarm

case node['bauherr']['location']
when 'local'
  # run on local machine
  include_recipe 'bauherr::_local'
when 'swarm'
  # run on the swarm cluster node
  include_recipe 'bauherr::_swarm'
end

#########################################################
# NOTES
#########################################################
### setup omnibus (only host machine to utilize cache!)
# https://supermarket.chef.io/cookbooks/omnibus
# https://hub.docker.com/r/tragus/chef-omnibus/dockerfile
# ? what if we want to build for different platforms ?
# so it is better to use omnibus in docker container
# and make that container persistent to use cache
# this will give us isolated environments!!!
# this actually is required because omnibus uses /opt/tao
####

######################
### DEFAULT RECIPE ###
######################



### create web app to represent the builds status
# todo

#################
### RESOURCES ###
#################

### create user

### setup omnibus project
# prepare project structure from templates
# use isolated environments for each customer (create dedicated linux user)
# start omnibus container with mounted omnibus project directory

### start omnibus build
# start build process with docker exec from under customer dedicated linux user account
# build process results in the package saved to exported volume
# get package name (and create ipfs hash? or place into some temporary repository Lagerhaus?)

### build container
# build container with the package installed
# ? omnibus to build docker

### upload docker image to registry
#

##### Rotenplatz ######
### ---  start verification process ----

### deploy to Kubernetes cluster

### start QA job

### wait for QA manual ?

##### Marketplatz ######
### --- release ---
# trigger release ?

### upload to S3 APT repository
### upload to Docker Registry for Google Cloud

# https://github.com/chef/chef-provisioning/issues/166#issuecomment-408606983
# chef-client -z --listen mycluster1.rb --lockfile /tmp/chef1.pid &
# chef-client -z --listen mycluster2.rb --lockfile /tmp/chef2.pid &
