name 'bauherr'
maintainer 'Serge A. Salamanka'
maintainer_email 'salamanka.serge@gmail.com'
license 'Apache v2.0'
description 'Installs/Configures Bauherr machine/cluster'
long_description 'Installs/Configures Bauherr machine/cluster'
version '0.2.0'
chef_version '>= 13.0'

# The `issues_url` points to the location where issues for this cookbook are
# tracked.  A `View Issues` link will be displayed on this cookbook's page when
# uploaded to a Supermarket.
#
# issues_url 'https://github.com/<insert_org_here>/bauherr/issues'

# The `source_url` points to the development repository for this cookbook.  A
# `View Source` link will be displayed on this cookbook's page when uploaded to
# a Supermarket.
#
# source_url 'https://github.com/<insert_org_here>/bauherr'

depends 'docker'
