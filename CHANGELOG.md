# bauherr CHANGELOG

This file is used to list changes made in each version of the bauherr cookbook.

# 0.2.0

- Add setup action for builder resource
- Rename omnibus recipe
- Add omnibus action for builder resource and remove omnibus recipe
- Add todos
- Add home attribute
- Add builder and authorized keys attributes
- Fix omnibus Dockerfile
- Create docker_network for bauherr builder user
- Update chefdk version
- Fix docker Gemfile

# 0.1.0

Initial release.
