name ENV['OMNIBUS_PROJECT']
package_name ENV['OMNIBUS_PACKAGE_NAME']
friendly_name ENV['OMNIBUS_FRIENDLY_NAME']
maintainer "CHANGE ME"
homepage "https://CHANGE-ME.com"

# Defaults to C:/test on Windows
# and /opt/test on all other platforms
install_dir "#{default_root}/#{name}"

# build_version Omnibus::BuildVersion.semver
build_version ENV['OMNIBUS_BUILD_VERSION']
build_iteration ENV['OMNIBUS_BUILD_ITERATION']
build_git_revision ENV['OMNIBUS_BUILD_GIT REVISION']


# Creates required build directories
dependency "preparation"

# test dependencies/components
dependency name
# dependency "php"

# Version manifest file
dependency "version-manifest"

exclude "**/.git"
exclude "**/bundler/git"
