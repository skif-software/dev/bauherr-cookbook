#
# Copyright 2014 Chef Software, Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

name "php"
default_version "7.1.24"

dependency "zlib"
dependency "pcre"
dependency "libxslt"
dependency "libxml2"
dependency "libiconv"
dependency "openssl"
#dependency "gd"

source url: "http://us.php.net/distributions/php-#{version}.tar.gz",
       md5: "6281561d8e46b9899484ca995ea44c3f"

relative_path "php-#{version}"

build do
  env = with_standard_compiler_flags(with_embedded_path)
  # https://stackoverflow.com/questions/51378551/compile-mbstring-statically-into-php
  command "./configure" \
          " --prefix=#{install_dir}/embedded" \
          " --enable-curl" \
          " --enable-json" \
          " --enable-gd" \
          " --enable-zip" \
          " --enable-tidy" \
          " --enable-xml" \
          " --enable-mbstring" \
          " --without-pear" \
          " --with-openssl" \
          " --with-openssl-dir=#{install_dir}/embedded" \
          " --with-zlib-dir=#{install_dir}/embedded" \
          " --with-pcre-dir=#{install_dir}/embedded" \
          " --with-libxml-dir=#{install_dir}/embedded", env: env
#tmp#          " --with-xpm-dir=no" \
#          " --with-zlib-dir=#{install_dir}/embedded" \
#          " --with-pcre-dir=#{install_dir}/embedded" \
#          " --with-xsl=#{install_dir}/embedded" \
#          " --with-libxml-dir=#{install_dir}/embedded" \
#          " --with-iconv=#{install_dir}/embedded" \
#          " --with-openssl-dir=#{install_dir}/embedded" \
#          " --with-gd=#{install_dir}/embedded", env: env
#          " --with-gd=#{install_dir}/embedded" \
#          " --enable-fpm" \
#          " --with-fpm-user=opscode" \
#          " --with-fpm-group=opscode", env: env

  make "-j #{workers}", env: env
  make "install", env: env
end
