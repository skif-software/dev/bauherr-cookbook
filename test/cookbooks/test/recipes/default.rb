#
# Cookbook:: test
# Recipe:: default

include_recipe 'bauherr::default'

builder = bauherr_builder node['bauherr']['builder'] do
  builder_home File.join(node['bauherr']['home'], node['bauherr']['builder'])
  builder_authorized_keys node['bauherr']['authorized']
  action [:setup, :omnibus, :create]
end

omnibus_env = {
  'OMNIBUS_PROJECT' => 'bauherr',
  'OMNIBUS_PACKAGE_NAME' => 'test-bauherr',
  # 'OMNIBUS_FRIENDLY_NAME' => 'Test package for Bauherr cookbook',
  'OMNIBUS_BUILD_VERSION' => Time.now.to_i.to_s,
  'OMNIBUS_BUILD_ITERATION' => 0,
  'OMNIBUS_BUILD_GIT_REVISION' => 'Unspecified'
}

bauherr_project 'test' do
  project_cookbook 'test'
  project_build_env omnibus_env
  project_builder lazy { builder }
  action :build
end
