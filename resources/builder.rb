property :builder_name, String, name_property: true
property :builder_home, String, required: true
property :builder_authorized_keys, Hash, default: {}
property :builder_images, Array, default: [
                                            {
                                              'name' => 'default',
                                              'platform' => 'ubuntu',
                                              'platform_version' => '16.04',
                                              'image_base' => nil
                                            }
                                          ]
property :images_updated, required: false, default: false
property :images,         required: false # exported property

include Bauherr::Helper

################################################################################
### SETUP BUILDER
################################################################################
#
action :setup do
  builder      = new_resource.builder_name
  builder_home = new_resource.builder_home
  builder_auth_keys = ""
  new_resource.builder_authorized_keys.each {|k, v| builder_auth_keys << v + "\n"}

  # see https://docs.chef.io/ohai.html
  # BUG: reloading only one plugin doesn't update node['etc']['passwd']
  # debug https://github.com/chef/ohai/blob/master/lib/ohai/plugins/passwd.rb
  # and report
  ohai 'reload' do
    action :nothing
  end

  user builder do
    comment 'Bauherr builder'
    home new_resource.builder_home
    manage_home true
    shell '/bin/bash'
    action :create
    notifies :reload, 'ohai[reload]', :immediately
  end

  group 'docker' do
    members [builder]
    append true
    action :manage
  end

  directory ::File.join(builder_home, '.ssh') do
    user builder
    group builder
    mode '0740'
    action :create
  end

  file ::File.join(builder_home, '.ssh', 'authorized_keys') do
    content builder_auth_keys
    user builder
    group builder
    mode '0640'
    action :create
  end

  docker_network builder do
    driver 'bridge'
    subnet lazy { user_subnet(user_id(builder)) }
    gateway lazy { user_gateway(user_id(builder)) }
    action :create
  end
end

################################################################################
### BUILD OMNIBUS IMAGES
################################################################################
#
## Builds different flavour omnibus docker images
action :omnibus do
  # Directory layout:
  # /home/name/omnibus/ - basic omnibus images
  # /home/name/builder/ - builder specific images
  # /home/name/project/ - project omnibus configuration
  # initialize run_state
  node.run_state['rebuild_omnibus_image'] = {}
  # prepare dockerfiles for omnibus images
  new_resource.builder_images.each do |image|
    [
      ::File.join(new_resource.builder_home, 'omnibus'),
      ::File.join(new_resource.builder_home, 'omnibus', image['name']),
      ::File.join(new_resource.builder_home, 'omnibus', image['name'], 'docker'),
      ::File.join(new_resource.builder_home, 'omnibus', image['name'], 'docker', image['platform']),
      ::File.join(new_resource.builder_home, 'omnibus', image['name'], 'docker', image['platform'], image['platform_version']),
      ::File.join(new_resource.builder_home, 'omnibus', image['name'], 'docker', image['platform'], image['platform_version'], 'files')
    ].each do |dir|
      directory dir do
        owner new_resource.builder_name
        group new_resource.builder_name
        mode '0755'
        action :create
      end
    end
    # Create files
    omnibus_docker_file_dir = ::File.join(new_resource.builder_home, 'omnibus', image['name'], 'docker', image['platform'], image['platform_version'])
    omnibus_docker_image_name = "#{new_resource.builder_name}/omnibus/#{image['name']}/#{image['platform']}"
    omnibus_docker_image_tag  = "#{image['platform_version']}"
    omnibus_docker_image      = "#{omnibus_docker_image_name}:#{omnibus_docker_image_tag}"
    node.run_state['rebuild_omnibus_image'][omnibus_docker_image] = false
    ruby_block "#{new_resource.builder_name}_rebuild_omnibus_image_#{image['name']}" do
      block do
        node.run_state['rebuild_omnibus_image'][omnibus_docker_image] = true
      end
      action :nothing
    end
    # Check if image platform is supported
    case image['platform']
    when 'ubuntu'
      package = 'deb'
    when 'debian'
      package = 'deb'
    # when 'redhat'
    #   package = 'rpm'
    else
      raise %Q[Unsupported platform '#{image['platform_version']}' specified in builder_images resource property.]
    end
    %w(Gemfile bundle.sh bash.sh build.sh).each do |f|
      template ::File.join(omnibus_docker_file_dir, 'files', f) do
        source "omnibus/docker/files/#{f}.erb"
        cookbook 'bauherr'
        owner new_resource.builder_name
        group new_resource.builder_name
        mode '0644'
        action :create
        notifies :run, "ruby_block[#{new_resource.builder_name}_rebuild_omnibus_image_#{image['name']}]", :immediately
      end
    end
    # Create Dockerfile
    template ::File.join(omnibus_docker_file_dir, 'Dockerfile') do
      source 'omnibus/docker/Dockerfile.erb'
      variables({
        image_base: image['image_base'],
        platform: image['platform'],
        platform_version: image['platform_version'],
        package: package,
        chefdk_version: node['bauherr']['chefdk']['version']
      })
      cookbook 'bauherr'
      owner new_resource.builder_name
      group new_resource.builder_name
      mode '0644'
      action :create
      notifies :run, "ruby_block[#{new_resource.builder_name}_rebuild_omnibus_image_#{image['name']}]", :before # just to be sure run_state updated properly
      notifies :build, "docker_image[#{omnibus_docker_image_name}]", :immediately
    end
    # Build docker image
    docker_image omnibus_docker_image_name do
      source omnibus_docker_file_dir
      tag omnibus_docker_image_tag
      read_timeout 3200
      write_timeout 3200
      # action :build_if_missing
      action lazy { node.run_state['rebuild_omnibus_image'][omnibus_docker_image] } ? :build : :build_if_missing
      notifies :prune, 'docker_image_prune[remove_old_omnibus_images]', :delayed
    end
    # Remove old omnibus images
    docker_image_prune 'remove_old_omnibus_images' do
      dangling true
      action :nothing
    end
  end
end

################################################################################
### CREATE BUILDERS FROM OMNIBUS IMAGES
################################################################################
#
action :create do
  # initialize export
  new_resource.images = []
  # create omnibus builders
  new_resource.builder_images.each do |image|
    # prepare dockerfiles for omnibus builders
    [
      ::File.join(new_resource.builder_home, 'builder'),
      ::File.join(new_resource.builder_home, 'builder', image['name']),
      ::File.join(new_resource.builder_home, 'builder', image['name'], 'docker'),
      ::File.join(new_resource.builder_home, 'builder', image['name'], 'docker', image['platform']),
      ::File.join(new_resource.builder_home, 'builder', image['name'], 'docker', image['platform'], image['platform_version'])
    ].each do |dir|
      directory dir do
        owner new_resource.builder_name
        group new_resource.builder_name
        mode '0755'
        action :create
      end
    end
    omnibus_docker_image = "#{new_resource.builder_name}/omnibus/#{image['name']}/#{image['platform']}:#{image['platform_version']}"
    # TODO: allow usage of custom Dockerfiles for builders
    file ::File.join(new_resource.builder_home, 'builder', image['name'], 'docker', image['platform'], image['platform_version'], 'Dockerfile') do
      content "FROM #{omnibus_docker_image}" + "\n" + "MAINTAINER CHEF Bauherr cookbook" + "\n"
      owner new_resource.builder_name
      group new_resource.builder_name
      mode '0644'
      action :create
    end
    builder_image = "#{new_resource.builder_name}_builder_#{image['name']}_#{image['platform']}_#{image['platform_version']}"
    docker_image builder_image do
      source ::File.join(new_resource.builder_home, 'builder', image['name'], 'docker', image['platform'], image['platform_version'])
      tag 'latest'
      action lazy { node.run_state['rebuild_omnibus_image'][omnibus_docker_image] } ? :build : :build_if_missing
      notifies :run, "ruby_block[redeploy_project_containers]", :immediately
    end
    # DEBUG: uncomment to debug run_state
    # execute "lazy" do
    #   command  lazy { "echo run_state #{node.run_state['rebuild_omnibus_image']}" }
    # end
    # redeploy project containers and prune old omnibus builders
    # NOTE: this property is used in project resource
    ruby_block 'redeploy_project_containers' do
      block do
        new_resource.images_updated = true
      end
      action :nothing
    end
    # export image name
    new_resource.images << builder_image
  end
end
