property :project_name, String, name_property: true
property :project_source, String, default: 'omnibus'
property :project_cookbook, String, required: true
property :project_build_env, Hash, default: {}
property :project_builder,   required: true


################################################################################
### BUILD PROJECT
################################################################################
#
action :build do
  # create omnibus configuration
  remote_directory ::File.join(new_resource.project_builder.builder_home, 'project') do
    source new_resource.project_source
    cookbook new_resource.project_cookbook
    recursive true
    owner new_resource.project_builder.builder_name
    group new_resource.project_builder.builder_name
    mode '0755'
    action :create
  end
  directory ::File.join(new_resource.project_builder.builder_home, 'project', 'pkg') do
    recursive true
    owner 'root'
    group 'root'
    action :create
  end
  new_resource.project_builder.images.each do |image|
    docker_container image + "-" + new_resource.project_name do
      repo image
      tag 'latest'
      command '/bin/bash'
      # TODO: use id
      # user "#{new_resource.project_builder.builder_name}:#{new_resource.project_builder.builder_name}"
      tty true
      volumes [
        "#{::File.join(new_resource.project_builder.builder_home, 'project')}:/home/omnibus/project"
      ]
      action new_resource.project_builder.images_updated ? [:delete, :create, :start] : [:create, :start]
    end
    build_env = ""
    new_resource.project_build_env.each do |k, v|
      build_env += %Q[-e #{k}='#{v}' ]
    end
    execute "build_#{new_resource.project_name}_project" do
      command %Q[docker exec --workdir=/home/omnibus/project ] +
              %Q[#{build_env}] +
              %Q[#{image + "-" + new_resource.project_name} ] +
              %Q[/home/omnibus/build.sh #{new_resource.project_name}]
      action :run
      notifies :start, "docker_container[#{image + "-" + new_resource.project_name}]", :before
    end
    docker_container image + "-" + new_resource.project_name do
      action :stop
    end
  end
  # remove old omnibus builders
  docker_image_prune 'remove_old_omnibus_builders' do
    dangling true
    action new_resource.project_builder.images_updated ? :prune : :nothing
  end
end
#
################################################################################
### UPLOAD PROJECT
################################################################################
#
action :upload do
end
