default['bauherr']['docker']['install'] = false
# in case we are in a standalone environment Docker should be installed
default['bauherr']['docker']['version'] = '18.06.1'
# otherwise just check that Docker is at least the minimum version required
default['bauherr']['docker']['minimum_version'] = '18.06.1'
