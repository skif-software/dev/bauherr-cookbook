default['bauherr']['version']     = '0.2.0'
default['bauherr']['description'] = 'Builder for your systems @ SKIF-Software'
default['bauherr']['location']    = 'local'
default['bauherr']['home']        = '/home'
default['bauherr']['builder']     = 'bauherr'
default['bauherr']['authorized']  = {}
#
# default['spielplatz']['name']     = 'spielplatz' # should be as cookbook's name
# default['spielplatz']['fqdn']     = 'local'
# default['spielplatz']['user']     = nil
# # list of users to create in advance
# default['spielplatz']['users']    = []
# default['spielplatz']['port']     = '80'
# default['spielplatz']['url']      = "http://#{node['spielplatz']['fqdn']}:" +
#                                            "#{node['spielplatz']['port']}"
# default['spielplatz']['home']     = '/home'
# default['spielplatz']['git']      = '/srv/git'
# # list of apps to create
# default['spielplatz']['apps']     = []
# # gitweb app configuration
# default['spielplatz']['app']      = {}
# # hash for ssh public keys with unique keys
# default['spielplatz']['authorized'] = {}
#
# default['github_token']   = false
# default['commit_message'] = 'System state updated'
