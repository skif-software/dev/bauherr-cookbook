module Bauherr
  module Helper
    def user_id(username)
      node['etc']['passwd'][username]['uid']
    end

    def validate_username(username)
      if username.size.between?(2, 32) && username.count("^a-z0-9_-").zero?
        return true
      else
        raise %Q[ERROR: username must begin with a lower case letter, ] +
              %Q[followed by lower case letters, digits, underscores, or dashes.\n] +
              %Q[Usernames may only be up to 32 characters long and 2 characters at least.\n] +
              %Q[Please, edit node['bauherr']['builder'] attribute.]
      end
    end

    def user_subnet(user_id)
      return "10.#{user_id_to_subnet_digits(user_id)}.0/24"
    end

    def user_gateway(user_id)
      return "10.#{user_id_to_subnet_digits(user_id)}.1"
    end

    private

    def user_id_to_subnet_digits(user_id)
      a = user_id.to_s.chars
      if a.size > 6
        raise "ERROR: user_id is more then 6 digits."
      end
      if a.size < 4
        raise "ERROR: user_id is less then 4 digits."
      end
      s2 = a
      s1 = s2.shift(3)
      if s1.join().to_i > 255 || s2.join().to_i > 255
        raise "ERROR: user_id is a large integer to extract subnet."
      end
      return "#{s1.join()}.#{s2.join()}"
    end

  end
end
